﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Data;

namespace WcfOrdersInfo
{
    [ServiceContract]
    interface IContract
    {
        [OperationContract]
        List<string> GetInfo(string CurrentUserID);
    }
}
