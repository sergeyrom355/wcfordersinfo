﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using System.IO;
using System.Xml;
using MySql.Data.MySqlClient;
using System.Runtime.Serialization;
using System.Threading;


namespace WcfOrdersInfo
{
    public partial class WcfOrdersInfoForm : Form
    {
        string ConnectionString;
        string strUri;
        public static DataTable DtOrders;
        
        public WcfOrdersInfoForm()
        {
            InitializeComponent();
        }

        private void WcfOrdersInfoForm_Load(object sender, EventArgs e)
        {
            try
            {
                GetXmlSettings();
                OpenHost();
                LoadFromDB();
                SetTimer();
            }
            catch (Exception) { }
        }

        private void OpenHost()
        {
            //Указание где ожидать входящие сообщения
            Uri Address = new Uri(strUri);

            //Указание как обмениваться сообщениями
            BasicHttpBinding Binding = new BasicHttpBinding();

            //Указание контракта
            Type Contract = typeof(IContract);

            //Создание провайдера хостинга с указание сервиса
            ServiceHost Host = new ServiceHost(typeof(Service));

            //Добавление конечной точки
            Host.AddServiceEndpoint(Contract, Binding, Address);

            //Открываем хост (начало ожидания прихода сообщений)
            Host.Open();
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void GetXmlSettings()
        {
            //Получаем путь к xml файлу
            string path = Path.Combine(Directory.GetParent(Application.StartupPath).FullName, "settings.xml");
            if (!File.Exists(path))
            {
                MessageBox.Show("Не найден файл настроек!");
                Application.Exit();
            }

            //Создаем экземляр класса XmlDocument и загружаем в него файл xml
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(path);
            //Получаем корневой элемент mxl документа
            XmlElement xRoot = xDoc.DocumentElement;
            //Получаем узел 
            XmlNode xNodeUri = xRoot.SelectSingleNode("Uri");
            strUri = xNodeUri.InnerText;

            XmlNode xNodeRemoteServer = xRoot.SelectSingleNode("RemoteServer");
            //Создаем экземляр класса строящего строку и заполняем его поля данными из xml файла
            MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder();
            csb.Server = xNodeRemoteServer.SelectSingleNode("IP").InnerText;
            csb.UserID = xNodeRemoteServer.SelectSingleNode("DBUser").InnerText;
            csb.Password = xNodeRemoteServer.SelectSingleNode("DBPassword").InnerText;
            csb.Database = xNodeRemoteServer.SelectSingleNode("DBName").InnerText;
            csb.CharacterSet = "utf8";

            //Получаем созданную строку подключения
            ConnectionString = csb.ConnectionString;
        }

        private void LoadData(object Sender, EventArgs e)
        {
            LoadFromDB();
        }

        private void LoadFromDB()
        {
            try
            {
                DateTime CurDate = DateTime.Now.Date;
                if (DateTime.Now.Hour >= 0 && DateTime.Now.Hour < 4)
                {
                    CurDate = CurDate.AddDays(-1);
                }

                DtOrders = new DataTable("DtOrdersInfo");

                MySqlConnection Con = new MySqlConnection(ConnectionString);
                Con.Open();

                //Колличество и сумма всех оплаченных заказов за текущую смену
                string query = "select h.order_id, bar, user_id, " +
                    "if (coalesce(time_deletion, time_reject) != null, -1, " +
                    "if (time_pay is null,0,1)) completed, sum(t.sum) summ from orders_hat h " +
                    "left join(select* from orders_tab) t on h.order_id = t.order_id where h.date_sm ='" + CurDate.ToString("yyyy-MM-dd") +
                    "' group by h.order_id order by bar;";

                MySqlDataAdapter NewAdapter = new MySqlDataAdapter(query, Con);
                NewAdapter.Fill(DtOrders);
            }
            catch (Exception) { }
        }

        private void SetTimer()
        {
            System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
            timer.Interval = 60000;
            timer.Tick += new EventHandler(LoadData);
            timer.Enabled = true;
        }
    }
}
