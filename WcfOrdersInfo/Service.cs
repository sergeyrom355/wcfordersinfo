﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace WcfOrdersInfo
{
    class Service : IContract
    {
        public List<string> GetInfo(string CurrentUserID)
        {
            List<string> answer = new List<string>();
            try
            {
                //---Заказы всего
                //В работе
                answer.Add(WcfOrdersInfoForm.DtOrders.AsEnumerable().Count(row => (int)row["completed"] == 0).ToString());
                answer.Add(WcfOrdersInfoForm.DtOrders.AsEnumerable().Where(row => (int)row["completed"] == 0 && !string.IsNullOrEmpty(row["summ"].ToString())).Sum(row => Convert.ToDouble(row["summ"])).ToString());
                //Выполнены
                answer.Add(WcfOrdersInfoForm.DtOrders.AsEnumerable().Count(row => (int)row["completed"] == 1).ToString());
                answer.Add(WcfOrdersInfoForm.DtOrders.AsEnumerable().Where(row => (int)row["completed"] == 1 && !string.IsNullOrEmpty(row["summ"].ToString())).Sum(row => Convert.ToDouble(row["summ"])).ToString());

                //---Заказы для текущего пользователя
                //В работе
                answer.Add(WcfOrdersInfoForm.DtOrders.AsEnumerable().Count(row => (int)row["completed"] == 0 && row["user_id"].ToString() == CurrentUserID).ToString());
                answer.Add(WcfOrdersInfoForm.DtOrders.AsEnumerable().Where(row => row["user_id"].ToString() == CurrentUserID && (int)row["completed"] == 0 && !string.IsNullOrEmpty(row["summ"].ToString())).Sum(row => Convert.ToDouble(row["summ"])).ToString());
                //Выполнены
                answer.Add(WcfOrdersInfoForm.DtOrders.AsEnumerable().Count(row => (int)row["completed"] == 1 && row["user_id"].ToString() == CurrentUserID).ToString());
                answer.Add(WcfOrdersInfoForm.DtOrders.AsEnumerable().Where(row => row["user_id"].ToString() == CurrentUserID && (int)row["completed"] == 1 && !string.IsNullOrEmpty(row["summ"].ToString())).Sum(row => Convert.ToDouble(row["summ"])).ToString());

                //---Отклоненные и удаленные
                answer.Add(WcfOrdersInfoForm.DtOrders.AsEnumerable().Count(row => (int)row["completed"] == -1).ToString());
                answer.Add(WcfOrdersInfoForm.DtOrders.AsEnumerable().Where(row => (int)row["completed"] == -1 && !string.IsNullOrEmpty(row["summ"].ToString())).Sum(row => Convert.ToDouble(row["summ"])).ToString());

                return answer;
            }
            catch(Exception)
            {
                return answer;
            }
        }
    }
}
